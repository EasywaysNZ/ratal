Gem::Specification.new do |s|
	s.name = 'ratal'
	s.version = '0.1.0.0'
	s.date = '2017-03-10'
	s.summary = 'Ruby port of Project Katal'
	s.description = 'A ruby port of Project Katal, an unofficial interface to the undocumented Kamar API'
	s.authors = ['Campbell Suter']
	s.email = 'znix@znix.xyz'
	s.files = %w(ratal ratal/nethttp_provider ratal/request_provider).map { |s| "lib/#{s}.rb" }
	s.homepage   = 'https://gitlab.com/EasywaysNZ/ratal'
	s.license = 'GPL-3.0+'
end

