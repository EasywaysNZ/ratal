# Project RATAL
[![Gem Version](https://badge.fury.io/rb/ratal.svg)](https://badge.fury.io/rb/ratal)

Unofficial Ruby KAMAR API - 
Port of [Project katal](https://github.com/TGS-App/API)

## Usage
First, find your API location.
This is the location of your student portal.

Examples:
- portal.example.com
- student.example.com
- remote.example.com
- https://portal.example.com
- portal.example.com/api/api.php

Create the `ratal` object:

```
require 'ratal'
API='portal.example.com'

ratal = Ratal.new API
```

If you need to do anything restricted to teachers or students,
such as viewing the timetable, you have to log in:

```
ratal.connect 'Username', 'Password'
```

This currently returns a hash that contains the API key needed for 
future requests, as this functionality is WIP.
