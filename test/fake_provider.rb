class FakeProvider < RequestProvider
	def request(uri, headers, data)
		filename = data[:FileName]

		file = case filename
						 when 'ServerSettings'
							 'settings'
						 when 'Logon'
							 v = 'logon_bad'
							 v = 'logon_good' if data[:Username] == '12345' and data[:Password] == 'badpasswd'
							 v
						 else
							 raise "Unknown filename #{filename}"
					 end

		target = File.new "test/testdata/#{file}.xml"
		IO.read target
	end
end