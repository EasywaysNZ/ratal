require 'minitest/autorun'
require 'ratal'

class StaticTest < Minitest::Test
	def test_api_target
		correct_target = 'http://kamar.example.com/api/api.php'
		base_api = 'kamar.example.com'

		# Test without a trailing '/', with and without the http://
		assert_equal correct_target, Ratal.new("#{base_api}").api_target
		assert_equal correct_target, Ratal.new("http://#{base_api}").api_target

		# Test with a trailing '/', with and without the http://
		assert_equal correct_target, Ratal.new("#{base_api}/").api_target
		assert_equal correct_target, Ratal.new("http://#{base_api}/").api_target

		# Test with the /api/api.php, with and without the http://
		assert_equal correct_target, Ratal.new("#{base_api}/api/api.php").api_target
		assert_equal correct_target, Ratal.new("http://#{base_api}/api/api.php").api_target
	end
end
