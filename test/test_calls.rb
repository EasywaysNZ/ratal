require 'minitest/autorun'
require 'rexml/document'
require 'net/http'
require 'fake_provider'

class CallsTest < Minitest::Test
	def test_login
		target = {
				file_name: 'Logon',
				error_code: '0',
				success: 'YES',
				logon_level: '1',
				current_student: '12345',
				key: 'abcdefghijklmnopqrstuvwxyz123456'
		}
		result = mkratal.connect '12345', 'badpasswd'

		assert_equal target, result
	end

	def test_settings
		target = {
				file_name: 'ServerSettings',
				error_code: '0',
				settings_version: '1.03',
				mini_os_version: '2.14',
				min_android_version: '1.08',
				students_allowed: '1',
				staff_allowed: '1',
				students_saved_passwords: '1',
				staff_saved_passwords: '0',
				school_name: 'Nayland College',
				logo_path: 'http://student.nayland.school.nz/school-assets/logo.png',
				assessment_types_shown: 'ABCEGHKLMOPRSTUVY',
				show_enrolled_entries: '0',
				user_access: {
						user: {
								notices: '0',
								events: '0',
								details: '0',
								timetable: '0',
								attendance: '0',
								ncea: '0',
								results: '0',
								groups: '0',
								awards: '0',
								pastoral: '0'
						}
				}
		}
		result = mkratal.settings

		assert_equal target, result
	end

	protected
	def mkratal
		Ratal.new 'kamar.example.com', FakeProvider.new
	end
end
