class NetHTTPProvider < RequestProvider
	def request(uri, headers, data)
		# Convert the symbol keys to strings for the headers
		headers_mapped = {}
		headers.each { |k, v| headers_mapped[k.to_s] = v }

		# Convert the symbol keys to strings for the data
		data_mapped = {}
		data.each { |k, v| data_mapped[k.to_s] = v }

		# Make the connection
		http = Net::HTTP.new(uri.hostname, uri.port)

		# Build the request
		req = Net::HTTP::Post.new(uri.request_uri, headers_mapped)
		req.set_form_data(data_mapped)

		# Run the request
		response = http.request(req)

		# Return the result
		response.body
	end
end