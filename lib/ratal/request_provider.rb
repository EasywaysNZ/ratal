class RequestProvider
	METHOD = 'Method not implemented!'

	# Fire a HTTP request.
	# @param [URI] uri The URI to send the request to
	# @param [Hash] headers The headers to be sent
	# @param [Hash] data The parameters to be used on this request
	# @return [String] The result, as a string, of this request
	def request(uri, headers, data)
		raise METHOD
	end
end
