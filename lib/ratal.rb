require 'ratal/request_provider'
require 'ratal/nethttp_provider'

class Ratal
	USER_AGENT = 'KAMAR/1455 CFNetwork/758.4.3 Darwin/15.5.0'
	CONTENT_TYPE = 'application/x-www-form-urlencoded'

	@api_base = nil
	@api_target = nil
	@provider = nil

	def initialize(api_base, provider = NetHTTPProvider.new)
		@api_base = api_base
		@provider = provider

		api_string = 'api/api.php'

		@api_target = @api_base

		# Make sure it starts with http://
		@api_base.prepend 'http://' unless @api_target =~ /^[a-z]+:\/\//

		# If it already has the api string on it, skip it
		return if @api_target.end_with? api_string

		# Make sure it ends with '/'
		@api_base.concat '/' unless @api_target.end_with? '/'

		# Make sure it ends with api/api.php
		@api_base.concat api_string
	end

	def settings
		call_method 'GetSettings', 'ServerSettings', 'vtku'
	end

	def connect(username, password)
		call_method 'Logon', 'Logon', 'vtku', Username: username, Password: password
	end

	attr_reader :api_base
	attr_reader :api_target

	protected
	def call_method(command, target, key, options = {})
		params = {'Command': command, 'FileName': target, 'Key': key}
		str = call params.merge(options)

		doc = REXML::Document.new(str)

		result = process_element(doc.root)

		result
	end

	def call(data)
		# noinspection RubyStringKeysInHashInspection
		headers = {
				'User-Agent': USER_AGENT,
				'Content-Type': CONTENT_TYPE
		}

		uri = URI(api_target)
		@provider.request(uri, headers, data)
	end

	def process_element(elem)
		result = {}

		children = elem.elements
		if children.empty?
			return elem.text
		else
			children.each do |e|
				result[symbolize e.name] = process_element(e)
			end
		end

		result
	end

	# http://stackoverflow.com/a/1509939
	def symbolize(str)
		str.gsub(/::/, '/').
				gsub(/([A-Z]+)([A-Z][a-z])/, '\1_\2').
				gsub(/([a-z\d])([A-Z])/, '\1_\2').
				tr('- ', '_').
				downcase.
				to_sym
	end
end
